;;; mpasm-mode.el --- major mode for editing microchip pic assembler code

;; Version: 0.1
;; Apenas edição de asembly do Microchip PIC

;; Alguns trechos de código foram retirados do asm-mode.el do GNU Emacs.


;;; Code:

;; Arquivos
(require 'mpasm-indent)
(require 'mpasm-keywords)


(defgroup mpasm nil
  "Modo para edição de código assembly do MPASM."
  :group 'languages)

(defcustom mpasm-use-default-keybindings t
  "Use the default keybindings (See README) if non-nil."
  :type 'boolean
  :group 'mpasm)

(defcustom mpasm-mode-hook nil
  "Hook run when `mpasm-mode' is initialized."
  :type 'hook
  :group 'mpasm)

(defcustom mpasm-mode-verbose t
  "Verbose mode to mpasm"
  :type 'boolean
  :group 'mpasm)


(defvar mpasm-mode-syntax-table
  (let ((mpasm-syntax-table (make-syntax-table)))
    (modify-syntax-entry ?_  "w"     mpasm-syntax-table) ;   faz parte de uma palavra
    (modify-syntax-entry ?\# "w"     mpasm-syntax-table) ; # faz parte de uma palavra
    (modify-syntax-entry ?\' "w"     mpasm-syntax-table) ; ' faz parte de uma palavra
    (modify-syntax-entry ?\. "w"     mpasm-syntax-table) ; . faz parte de uma palavra
    (modify-syntax-entry ?<  "(>"    mpasm-syntax-table) ; < > par
    (modify-syntax-entry ?>  ")<"    mpasm-syntax-table) ;
    (modify-syntax-entry ?\; "<"     mpasm-syntax-table) ; Comment starter
    (modify-syntax-entry ?\n ">"     mpasm-syntax-table) ; Comment ender
    (modify-syntax-entry ?/  ". 124" mpasm-syntax-table)
    (modify-syntax-entry ?\" "\""    mpasm-syntax-table) ; String quote
    mpasm-syntax-table)
  "Syntax table for mpasm-mode")

(defvar mpasm-mode-map (make-keymap))
(defun mpasm-setup-default-keybindings ()
  (define-key mpasm-mode-map ";"     'mpasm-mode-comment)
  (define-key mpasm-mode-map "\C-c;" 'comment-region)   
  (define-key mpasm-mode-map "\C-j"  'newline-and-indent)
  (define-key mpasm-mode-map "\M-i"  'mpasm-extra-indent-keyword))

(defconst mpasm-mode-font-lock-keywords
  (list
   ;; --------------------------------------------------
   ;; --------------------------------------------------
   ;; Texto na margem esquerda terminados em (:) = Label
   `(,"^[a-z_]+\\(\\sw\\)*:"
     (0	font-lock-function-name-face t))
   ;; --------------------------------------------------
   ;; Texto na margem esquerda terminados em (:) = Label
   `(,(concat "^\\([a-z_]+\\(\\sw\\)\\)[ \t]*"
              ;;(regexp-opt '("org" "code" "macro" "udata" "udata_shr") 'words))
              "\\<\\(code\\|macro\\|org\\|udata\\(?:_shr\\)?\\)\\>")
     (1	font-lock-function-name-face t))
   ;; --------------------------------------------------
   ;; Texto na margem esquerda, label de variavel
   `(,(concat "^\\(\\(\\sw\\)+\\)[ \t]*"
              ;;(regexp-opt '("equ" "set" ".set" "res" "db" "de" "dt" "dw") 'words))
              "\\<\\(\\.set\\|d[betw]\\|equ\\|res\\|set\\)\\>")
     (1	font-lock-variable-name-face t))
   ;; --------------------------------------------------
   ;; include
   `(,(concat "^[ \t]+"
              ;; (regexp-opt '("#include" "include") 'words)
              "\\<\\(#?include\\)\\>"
              "\\([ \t]+[<\"]?\\(\\sw\\|\\.\\|\\/\\|\\-\\)*[>\"]?\\)")
     (2 font-lock-type-face t))
   ;; define e undefined
   `(,"^[ \t]+\\<\\(#\\(?:\\(?:un\\)?define\\)\\)\\>\\([ \t]+\\<\\(\\sw\\)+\\>\\)"
     (2 font-lock-variable-name-face t))
   ;; res e equ
   `(,"^\\([ \t]+\\(\\sw\\)+\\)[ \t]+\\(equ\\|res\\|d[betw]\\)\\>"
     (1 font-lock-variable-name-face nil t))
   ;; __config, __fuses, __idlocs e config
   `(,"\\<\\(__\\(?:config\\|\\(?:fuse\\|idloc\\)s\\)\\|config\\)+\\>" "\\<\\(\\sw\\|\\(&\\)\\|\\s-\\)+\\>" nil nil
     (0 font-lock-type-face t))
   ;; argumentos de mnemonicos
   ;;`(,(regexp-opt mpasm-mode-keywords-all 'words) "\\<\\(\\sw\\|[, ]\\)+\\>" nil nil
   ;;(0 font-lock-variable-name-face nil t))
   ;; 
   `(,(regexp-opt mpasm-mode-keywords-mnemonics 'words)       .	font-lock-keyword-face)
   `(,(regexp-opt mpasm-mode-keywords-preprocessor 'words)    .	font-lock-preprocessor-face)
   `(,(concat "\\<" mpasm-radix-re "\\>")                     . font-lock-warning-face)
   ;; Pseudo instructions
   `(,(regexp-opt mpasm-mode-keywords-pseudo 'words)          .	font-lock-keyword-face)
   ;; Pic 18
   `(,(regexp-opt mpasm-mode-keywords-mnemonics-pic18 'words) . font-lock-keyword-face)
   ;; Register names
   `(,(regexp-opt mpasm-mode-registers 'words)                . font-lock-constant-face)
   ;; Bit Names
   `(,(regexp-opt mpasm-mode-bit-names 'words)                . font-lock-constant-face)
   ;; Arithmetic operators
   `(,(regexp-opt mpasm-arithmetic-operators)                 . font-lock-warning-face)
   ;; W e F
   `(,(regexp-opt '("w" "f") 'words)                          . font-lock-warning-face)
   
   ) "Highlight for mpasm mode" ) ; end defconst mpasm-mode-font-lock-keywords

(defvar mpasm-mode-name "[PIC MPASM]")

;;;###autoload
(defun mpasm-mode ()
"Major mode for editing microchip PIC assembler code.
Features a private abbrev table and the following bindings:

\\[asm-colon]\toutdent a preceding label, tab to next tab stop.
\\[tab-to-tab-stop]\ttab to next tab stop.
\\[asm-newline]\tnewline, then tab to next tab stop.
\\[asm-comment]\tsmart placement of assembler comments.

The character used for making comments is ';').

Turning on MPASM mode runs the hook `mpasm-mode-hook' at the end of initialization.

Special commands:
\\{mpasm-mode-map}"
  (interactive)
  (kill-all-local-variables)
  (set-syntax-table mpasm-mode-syntax-table)
  (use-local-map mpasm-mode-map)
  ;;
  (setq major-mode 'mpasm-mode)
  (setq mode-name mpasm-mode-name)
  ;; 
  (set (make-local-variable 'font-lock-defaults)   '(mpasm-mode-font-lock-keywords nil t))
  (set (make-local-variable 'font-lock-keywords)   '(mpasm-mode-font-lock-keywords))
  (set (make-local-variable 'indent-line-function) 'mpasm-mode-indent-line)
  ;; 
  (set (make-local-variable 'comment-start) ";")
  (set (make-local-variable 'comment-padding) ";; ")
  ;;
  (mpasm-setup-default-keybindings)
  ;; 
  (setq-default indent-tabs-mode nil) ; insert spaces for indentation
  ;;
  (setq mpasm-tab-list (number-sequence mpasm-indent-column-arg 100 8))
  (add-to-list 'mpasm-tab-list mpasm-indent-column-mnemonic)
  (setq-default tab-stop-list mpasm-tab-list)
  ;;
  (setq-default fill-column 100)
  ;; Desabilita Case sensitive para busca
  (setq-default case-fold-search t)
  ;; Desabilita Case sensitive para o Font lock
  (setq-default font-lock-keywords-case-fold-search t)
  ;;
  (run-hooks 'mpasm-mode-hook))
;; -------------------------------------------------------------------------------------------------
(provide 'mpasm-mode)
;; -------------------------------------------------------------------------------------------------
;; mpasm-mode.el ends here
;; -------------------------------------------------------------------------------------------------
;; Local Variables:
;; coding: utf-8
;; End:
